# VocalArpeggiator

## Configuration pour le de developpement
+ Loader les sous modules git 
    + iPlug2 
    + vst3sdk
+ Créer le folder
    `C:\Program Files\Common Files\VST3`
+ Visual Studio 2019 (toujours en mode administrateur !!!).

### Pour le debugging
[Telecharger VST 3 Audio Plug-Ins SDK](https://www.steinberg.net/en/company/developers.html)

Ouvrir le .zip et aller chercher le zip `VST3PluginTestHost_x64_Installer_2.8.0.zip` dans le repertoire
 `/VST_SDK/VST3_SDK/bin/WINDOWS_64BIT/`.
Extraire et installer.

## Notes

### iPlug 2
On utilise le framework de création de plugin audio : iPlug2

[Requirements pour le développement iPlug2](https://iplug2.github.io/docs/md_examples.html) 

[Examples de projet Iplug2](https://iplug2.github.io/) 

### Format des plugins
Avec IPlug2, on peut créer des plugins dans plusieurs formats.

Sur Windows, on peut créer des plugins de format : AAX, VST et des applications standalone (.exe).

Le format VST se retrouve à être le format le plus supporté dans les stations audionumériques. Nous allons donc accorder notre effort à créer un plugin VST3.

Toutefois, si nous désirons lancer notre plugin sur tout les plateformes, il faudrait également effectuer les configurations nécessaire pour le format AAX (utilisé uniquement par `Pro Tools`) et configurer un poste sur Mac pour créer des plugins Audio Unit (utilisé notamment sur le logiciel exclusif à MacOs `Logic Pro`.) 

## Comment utilisé le plugin VST
Une fois le projet buildé, dans le dossier `build-win`, une application `.exe` est créé ainsi qu'un fichier `.vst3`.
Celui-ci peut être déplacé dans le folder de plugin utilisé par la station audio-numérique.

### Inno Setup 
Pour créer les installers. : https://jrsoftware.org/isinfo.php#features